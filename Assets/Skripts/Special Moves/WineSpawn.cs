using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WineSpawn : MonoBehaviour
{
    public float reSpawnTimer;
    [SerializeField] private GameObject collectable;
    private TESTBattleStats wine;
    //[HideInInspector]public GameObject playerCharacter;
    public GameManager _WGameManager;
    public void PullTriggerW (Collider other)
    {
       // playerCharacter = other.gameObject;
       _WGameManager.ChangeHealthBAr(50,true);
        collectable.SetActive(false);
        StartCoroutine(SpawnDelay());

        

        IEnumerator SpawnDelay()
        {

            yield return new WaitForSeconds(reSpawnTimer);
            collectable.SetActive(true);


        }
    }
}

