using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class ReSpawnPoint : MonoBehaviour
{
    [HideInInspector] public Vector3 reSpawnpoint;
    [HideInInspector] public GameObject reSpawnGameObject;
    private GameObject rePlayerCharacter;
    public GameManager _manager;
    private bool checkFight;
    private bool checkWon;
    public BattleScriptableObject _battleScriptable;
    
    private void Start()
    {
        rePlayerCharacter = gameObject;
        checkFight = _battleScriptable.fight;
        checkWon = _battleScriptable.won;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Spawnpoint"))
        {
            
            reSpawnGameObject = other.gameObject;
            reSpawnpoint = reSpawnGameObject.transform.position;
            

        }
    }

    private void Update()
    {
        //2te respawn methode fehlt noch
        if (checkFight==true && checkWon == false)
        {
            rePlayerCharacter.transform.position = reSpawnpoint;
            _manager.HealUp();
            //hp up
            //reHealth += 50;
        }
    }
}

