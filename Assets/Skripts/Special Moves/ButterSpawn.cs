using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ButterSpawn : MonoBehaviour
{
    public float reSpawnTimer;
    [SerializeField] private GameObject collectable;
    private TESTBattleStats butter;
    //[HideInInspector] public GameObject playerCharacter;
     public GameManager _BGameManager;
    
    public void PullTrigger (Collider other)
    {
      //  playerCharacter = other.gameObject;
        _BGameManager.ChangeButterBar(50,true);
        collectable.SetActive(false);
        StartCoroutine(SpawnDelay());

        

        IEnumerator SpawnDelay()
        {

            yield return new WaitForSeconds(reSpawnTimer);
            collectable.SetActive(true);


        }
    }
}

    
    

