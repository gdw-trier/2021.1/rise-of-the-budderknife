using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class PcTurn : MonoBehaviour
{
  private GameObject turnPlayerCharacter;
  //prüft ob bereit geturned wurde
  private bool didTurn = false;
  private void Start()
  {
    turnPlayerCharacter = gameObject;
  }

  private void OnTriggerEnter(Collider other)
  {
    if (other.gameObject.CompareTag("Turn"))
    {
      if (didTurn == false)
      {
        turnPlayerCharacter.transform.Rotate(0,90,0);
        didTurn = true;
      }
      else
      {
        turnPlayerCharacter.transform.Rotate(0,-90,0);
        didTurn = false;
      }
    }

  }
}
