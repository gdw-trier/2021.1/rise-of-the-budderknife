using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
   public GameObject tpPlayerCharacter;
   [SerializeField] private Transform tpLocation;
   
   
   private void OnTriggerEnter(Collider other)
   {
      if (other.gameObject.CompareTag("Player"))
      {
         tpPlayerCharacter = other.gameObject;
         tpPlayerCharacter.transform.position = tpLocation.transform.position;
         //Enter CutScene
         Destroy(gameObject);
      }
   }

}
