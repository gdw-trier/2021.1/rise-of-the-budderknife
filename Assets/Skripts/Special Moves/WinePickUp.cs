using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinePickUp : MonoBehaviour
{
    //Muss noch auf die richtigen Scripts eingestellt werden
    public GameObject playerCharacter;
    private TESTBattleStats butter;


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            gameObject.GetComponentInParent<WineSpawn>().PullTriggerW(other);

        }
    }
}

