using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattlePlayerInput : MonoBehaviour
{
    public GameObject attack, heal, butter, saw, jump;
    public Color selectColorRight;
    public Color selectColorLeft;
    public Color standardColorRight;
    public Color standardColorLeft;
    public int health;
    public int healFactor;
    public Text hpChangeText;
    public Text hpText;
    public Text butterText;
    public int strength;
    public int butterMana;
    public int[] manaCost = new int[5];

    private BattleManager manager;
    private bool attackSelected;
    private bool healSelected;
    private GameObject player;
    private float currentHealth;
    private float currentButter;
    private bool finishedTurn = false;
    private bool canUseButter = true;
    private bool canUseSaw = true;
    private bool canUseJump = true;

    // Start is called before the first frame update
    void Start()
    {
        manager = this.gameObject.GetComponent<BattleManager>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetHealthAndButter(int h, int b)
    {
        currentHealth = health;
        currentButter = butterMana;
        ChangeHealthBarTo(health - h);
        ChangeButterBarTo(butterMana - b);
        currentButter -= (butterMana - b);
        butterText.text = currentButter.ToString();
    }

    public void TakeDamage(int damage)
    {
        Debug.Log("Current Health: " + currentHealth);
        ChangeHealthBarTo(damage);
        ChangeText(damage, true);
        if(currentHealth <= 0)
        {
            manager.LostBattle();
        }
    }

    private void ChangeHealthBarTo(int damage)
    {
        player = GameObject.FindGameObjectWithTag("Player");
        Image[] healthBar = player.GetComponentsInChildren<Image>();
        float x = healthBar[2].gameObject.GetComponent<RectTransform>().rect.width;
        float temp = (float)damage / (float)currentHealth * x;
        x -= (int)temp;
        currentHealth -= damage;
        healthBar[2].gameObject.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, x);
        hpText.text = currentHealth.ToString();
    }

    private void ChangeButterBarTo(int butter)
    {
        player = GameObject.FindGameObjectWithTag("Player");
        Image[] healthBar = player.GetComponentsInChildren<Image>();
        float x = healthBar[5].gameObject.GetComponent<RectTransform>().rect.width;
        float y = healthBar[4].gameObject.GetComponent<RectTransform>().rect.width;
        float temp = (float)butter / (float)currentButter * x;
        x -= (int)temp;
        if (currentButter > butterMana) { currentButter = butterMana; x = y; }
        healthBar[5].GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, x);
        butterText.text = currentButter.ToString();
    }

    private bool UseButter(int attackType)
    {
        Debug.Log("Current Butter: " + currentButter);
        if ((currentButter -= manaCost[attackType]) < 0)
        {
            currentButter += manaCost[attackType];
            return false;
        }
        ChangeButterBarTo(manaCost[attackType]);
        return true;
    }

    public void OnConfirmAction()
    {
        if (attackSelected)
        {
            bool x = UseButter(manager.GetAttackType()+1);
            if (!x)
            {
                OnStopAction();
                return;
            }
            manager.AttackSelectedEnemy(strength);
            manager.SetTurnOver(true);
        }
        else if (healSelected)
        {
            bool z = UseButter(0);
            if(!z)
            {
                OnStopAction();
                return;
            }
            player = GameObject.FindGameObjectWithTag("Player");
            Image[] healthBar = player.GetComponentsInChildren<Image>();
            float x = healthBar[2].gameObject.GetComponent<RectTransform>().rect.width;
            float y = healthBar[1].gameObject.GetComponent<RectTransform>().rect.width;
            float temp = (float)healFactor / (float)health * y;
            x += (int)temp;
            currentHealth += healFactor;
            if (currentHealth > health) { currentHealth = health; x = y; }
            healthBar[2].GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, x);
            hpText.text = currentHealth.ToString();
            ChangeText(healFactor, false);
            manager.SetTurnOver(true);
        }
    }

    public void FinishedWaiting(int currentEnemy)
    {
        hpChangeText.text = "";
        if(currentEnemy == 0)
        {
            DeselectOptions();
        }
    }

    private void ChangeText(float hpChange, bool isDamage)
    {
        string color = isDamage ? "red" : "green";
        string hp = "\n<color=" + color + ">" + (int)hpChange + "</color>";
        hpChangeText.text += hp;
    }

    //Deal with player Input
    public void OnStopAction()
    {
        manager.DeselectEnemies();
        DeselectOptions();
    }

    public void DeselectOptions()
    {
        attack.GetComponent<Image>().color = standardColorRight;
        heal.GetComponent<Image>().color = standardColorRight;
        butter.GetComponent<Image>().color = standardColorLeft;
        saw.GetComponent<Image>().color = standardColorLeft;
        jump.GetComponent<Image>().color = standardColorLeft;
        attackSelected = false;
        healSelected = false;
        manager = this.gameObject.GetComponent<BattleManager>();
        Debug.Log(manager);
        manager.DeselectAction();
    }

    public void OnAttack()
    {
        if (attackSelected) { return; }
        DeselectOptions();
        attackSelected = true;
        attack.GetComponent<Image>().color = selectColorRight;
        manager.SelectEnemy(false);
        manager.SetAttackType(0);
    }

    public void OnHeal()
    {
        if (attackSelected) { return; }
        if(!canUseButter) { return; }
        DeselectOptions();
        healSelected = true;
        heal.GetComponent<Image>().color = selectColorRight;
    }

    public void OnButterAttack()
    {
        if (attackSelected) { return; }
        if (!canUseButter) { return; }
        DeselectOptions();
        attackSelected = true;
        butter.GetComponent<Image>().color = selectColorLeft;
        manager.SelectEnemy(false);
        manager.SetAttackType(1);
    }

    public void OnSawAttack()
    {
        if (attackSelected) { return; }
        if (!canUseSaw) { return; }
        DeselectOptions();
        attackSelected = true;
        saw.GetComponent<Image>().color = selectColorLeft;
        manager.SelectEnemy(false);
        manager.SetAttackType(2);
    }

    public void OnJumpAttack()
    {
        if (attackSelected) { return; }
        if (!canUseJump) { return; }
        DeselectOptions();
        attackSelected = true;
        jump.GetComponent<Image>().color = selectColorLeft;
        manager.SelectEnemy(false);
        manager.SetAttackType(3);
    }

    public void OnSelectTargetUp()
    {
        if (attackSelected)
        {
            manager.SelectEnemy(true);
        }
    }

    public void OnSelectTargetDown()
    {
        if (attackSelected)
        {
            manager.SelectEnemy(false);
        }
    }

    public float GetCurrentHealth()
    {
        return currentHealth;
    }

    public float GetCurrentButter()
    {
        return currentButter;
    }

    public void SetButterUse(bool use)
    {
        canUseButter = use;
        heal.SetActive(use);
        butter.SetActive(use);
    }

    public void SetSawUse(bool use)
    {
        canUseSaw = use;
        saw.SetActive(use);
    }

    public void SetJumpUse(bool use)
    {
        canUseJump = use;
        jump.SetActive(use);
    }
}
