using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class II_BattleEnemy : MonoBehaviour
{
    public Image border, background, metal;
    public GameObject healthBar;
    public Camera battleCam;
    public Text hpChangeText;
    public Text hpText;
    public GameObject butteredImage, bleedingImage;

    private BattleManager battleManager;
    private Texture2D image;
    private int health;
    private float currentHealth;
    private int strenght;
    private int initiative;
    private int type;
    private bool turnFinished = false;
    private int accuracy = 5;
    private int battleTurns = 0;
    private int accuracyChangedTurn = 0;
    private bool bleeding = false;
    private int bleedingStartedTurn = 0;
    private float position;
    // 0 = Zahnstocher, 1 = Korken, 2 = Tape, 3 = Korkenzieher
    private int kind;

    // Start is called before the first frame update
    void Start()
    {
        battleManager = GameObject.FindGameObjectWithTag("BattleManager").GetComponent<BattleManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetUpBattleEnemy(Texture2D enemyImage, int enemyHealth, int enemyStrength, int enemyInitiative, int enemyType, int enemyKind)
    {
        image = enemyImage;
        health = enemyHealth;
        currentHealth = enemyHealth;
        strenght = enemyStrength;
        initiative = enemyInitiative;
        type = enemyType;
        kind = enemyKind;
        hpText.text = currentHealth.ToString();
    }

    public void SetPosition( float enemyPosition)
    {
        position = enemyPosition;
        SetHealthBarPosition(enemyPosition);
        hpText.text = currentHealth.ToString();
    }

    public Vector3 GetHealthBarPosition()
    {
        return healthBar.transform.position;
    }

    public int GetEnemyType()
    {
        return type;
    }

    public float GetEnemyPosition()
    {
        return position;
    }

    public void SetSelected(Color c)
    {
        border.color = c;
    }

    public void TakeDamage(float damage)
    {
        float temp = damage / health * 200;
        float x = metal.gameObject.GetComponent<RectTransform>().rect.width;
        x -= temp;
        currentHealth -= (int)damage;
        metal.gameObject.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, x);
        ChangeText(damage, true);
    }

    public void FinishedWaiting()
    {
        hpChangeText.text = "";
        if (currentHealth <= 0)
        {
            battleManager.DefeatedEnemy(position);
            Destroy(this.gameObject);
        }
    }

    private void ChangeText(float hpChange, bool isDamage)
    {
        string color = isDamage ? "red" : "green";
        string hp = "\n<color=" + color + ">" + (int)hpChange + "</color>";
        hpChangeText.text += hp;
        hpText.text = currentHealth.ToString();
    }

    private void SetHealthBarPosition(float position)
    {
        battleCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        Vector3 screenPos = battleCam.WorldToScreenPoint(this.gameObject.transform.position);
        screenPos.y -= (200 * position);
        screenPos.x -= 20;
        healthBar.transform.position = screenPos;
    }

    public void SetAccuracy(int i)
    {
        accuracy = i;
        if (i != 3)
        {
            accuracyChangedTurn = battleTurns;
            butteredImage.SetActive(true);
        }
    }

    public void SetBleeding(bool i)
    {
        bleeding = i;
        if (i)
        {
            bleedingStartedTurn = battleTurns;
            bleedingImage.SetActive(true);
        }
    }

    public void DecideOnAction()
    {
        bool finished = false;
        do
        {
            int i = Random.Range(0, accuracy);
            if (i > 0 && i < 4)
            {
                finished = AttackPlayer();
            }
            else if (i < 1 && currentHealth < health)
            {
                switch (kind)
                {
                    case 0:
                        finished = false;
                        break;
                    case 1:
                        finished = Heal();
                        break;
                    case 2:
                        finished = Heal();
                        break;
                    case 3:
                        finished = Heal();
                        break;
                }
            }
            else if (i == 4)
            {
                finished = SecondAttackPlayer();
            }
            else if (i >= 5)
            {
                finished = true;
                hpChangeText.text = "Miss";
            }
        } while (!finished);
        if (bleeding)
        {
            int dam = currentHealth > 5 ? 5 : (1 - (int)currentHealth) * (-1);
            TakeDamage(dam);
        }
        if ((battleTurns - accuracyChangedTurn) > 2) { accuracy = 3; butteredImage.SetActive(false); }
        if ((battleTurns - bleedingStartedTurn) > 3) { bleeding = false; bleedingImage.SetActive(false); }
        battleTurns++;
    }

    private bool AttackPlayer()
    {
        switch(kind)
        {
            case 0:
                gameObject.GetComponent<MafiaZahnstocher>().AttackOne();
                break;
            case 1:
                gameObject.GetComponent<BetrunkenerKorken>().AttackOne();
                break;
            case 2:
                gameObject.GetComponent<CobraTape>().AttackOne();
                break;
            case 3:
                gameObject.GetComponent<KaeferKorkenzieher>().AttackOne();
                break;
        }
        battleManager.AttackPlayer(strenght, type);
        return true;
    }

    private bool SecondAttackPlayer()
    {
        switch (kind)
        {
            case 0:
                gameObject.GetComponent<MafiaZahnstocher>().AttackTwo();
                battleManager.AttackPlayer((strenght + 5), type);
                break;
            case 1:
                return false;
            case 2:
                gameObject.GetComponent<CobraTape>().AttackTwo();
                battleManager.AttackPlayer((strenght + 10), type);
                break;
            case 3:
                gameObject.GetComponent<KaeferKorkenzieher>().AttackTwo();
                battleManager.AttackPlayer((strenght + 10), type);
                break;
        }
        return true;
    }

    private bool Heal()
    {
        if (currentHealth == health)
        {
            return false;
        }
        else
        {
            float temp = 30.0f / (float)health * 200.0f;
            float x = metal.gameObject.GetComponent<RectTransform>().rect.width;
            float y = background.gameObject.GetComponent<RectTransform>().rect.width;
            x += temp;
            currentHealth += 30;
            if (currentHealth > health)
            {
                currentHealth = health;
                x = y;
            }
            metal.gameObject.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, x);
            ChangeText(30, false);
            return true;
        }
    }
}
