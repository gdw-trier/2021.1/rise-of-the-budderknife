using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattlePlayerInput1 : MonoBehaviour
{
    public Image attack, heal, butter, saw, jump;
    public Color selectColor;
    public Color standardColor;
    public int health;
    public int healFactor;
    public Text hpChangeText;
    public Text hpText;
    public int strength;

    private II_BattleManager manager;
    private bool attackSelected;
    private bool healSelected;
    private GameObject player;
    private float currentHealth;
    private bool finishedTurn = false;

    // Start is called before the first frame update
    void Start()
    {
        manager = this.gameObject.GetComponent<II_BattleManager>();
        player = GameObject.FindGameObjectWithTag("Player");
        currentHealth = health;
        hpText.text = currentHealth.ToString();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TakeDamage(float damage)
    {
        player = GameObject.FindGameObjectWithTag("Player");
        Image[] healthBar = player.GetComponentsInChildren<Image>();
        float x = healthBar[2].gameObject.GetComponent<RectTransform>().rect.width;
        float y = healthBar[1].gameObject.GetComponent<RectTransform>().rect.width;
        float temp = damage / health * y;
        x -= temp;
        currentHealth -= (int)damage;
        healthBar[2].gameObject.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, x);
        ChangeText(damage, true);
    }

    public void OnConfirmAction()
    {
        if (attackSelected)
        {
            manager.AttackSelectedEnemy(strength);
            manager.SetTurnOver(true);
        }
        else if (healSelected)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            Image[] healthBar = player.GetComponentsInChildren<Image>();
            float x = healthBar[2].gameObject.GetComponent<RectTransform>().rect.width;
            float y = healthBar[1].gameObject.GetComponent<RectTransform>().rect.width;
            float temp = (float)healFactor / (float)health * y;
            Debug.Log("X: " + y + ", Temp: " + temp + ", currentHealth: " + currentHealth + ", healFactor: " + healFactor);
            x += temp;
            currentHealth += healFactor;
            Debug.Log("X: " + x + ", Temp: " + temp + ", currentHealth: " + currentHealth + ", healFactor: " + healFactor);
            if (currentHealth > health) { currentHealth = health; x = y; }
            healthBar[2].GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, x);

            ChangeText(healFactor, false);
            manager.SetTurnOver(true);
        }
    }

    public void FinishedWaiting(int currentEnemy)
    {
        hpChangeText.text = "";
        if(currentEnemy == 0)
        {
            DeselectOptions();
        }
    }

    private void ChangeText(float hpChange, bool isDamage)
    {
        string color = isDamage ? "red" : "green";
        string hp = "\n<color=" + color + ">" + (int)hpChange + "</color>";
        hpChangeText.text += hp;
        hpText.text = currentHealth.ToString();
    }

    public void OnStopAction()
    {
        manager.DeselectEnemies();
        DeselectOptions();
    }

    public void DeselectOptions()
    {
        attack.color = standardColor;
        heal.color = standardColor;
        butter.color = standardColor;
        saw.color = standardColor;
        jump.color = standardColor;
        attackSelected = false;
        healSelected = false;
        manager = this.gameObject.GetComponent<II_BattleManager>();
        Debug.Log(manager);
        manager.DeselectAction();
    }

    public void OnAttack()
    {
        
        if (attackSelected) { return; }
        Debug.Log("OnAttack");
        DeselectOptions();
        attackSelected = true;
        attack.color = selectColor;
        manager.SelectEnemy(false);
        manager.SetAttackType(0);
    }

    public void OnHeal()
    {
        if (attackSelected) { return; }
        DeselectOptions();
        healSelected = true;
        heal.color = selectColor;
    }

    public void OnButterAttack()
    {
        if (attackSelected) { return; }
        DeselectOptions();
        attackSelected = true;
        butter.color = selectColor;
        manager.SelectEnemy(false);
        manager.SetAttackType(1);
    }

    public void OnSawAttack()
    {
        if (attackSelected) { return; }
        DeselectOptions();
        attackSelected = true;
        saw.color = selectColor;
        manager.SelectEnemy(false);
        manager.SetAttackType(2);
    }

    public void OnJumpAttack()
    {
        if (attackSelected) { return; }
        DeselectOptions();
        attackSelected = true;
        jump.color = selectColor;
        manager.SelectEnemy(false);
        manager.SetAttackType(3);
    }

    public void OnSelectTargetUp()
    {
        if (attackSelected)
        {
            manager.SelectEnemy(true);
        }
    }

    public void OnSelectTargetDown()
    {
        if (attackSelected)
        {
            manager.SelectEnemy(false);
        }
    }
}
