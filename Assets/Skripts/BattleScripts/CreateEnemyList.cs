using UnityEngine;
using UnityEditor;

#if UNITY_EDITOR
public class CreateEnemyList
{
    [MenuItem("Assets/Create/EnemyList")]
    public static EnemyList Create()
    {
        EnemyList asset = ScriptableObject.CreateInstance<EnemyList>();

        AssetDatabase.CreateAsset(asset, "Assets/Data/EnemyList.asset");
        AssetDatabase.SaveAssets();
        return asset;
    }
}
#endif
