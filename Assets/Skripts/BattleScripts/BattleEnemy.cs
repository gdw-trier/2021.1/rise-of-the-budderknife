using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleEnemy : MonoBehaviour
{
    public Image border, background, metal;
    public GameObject healthBar;
    public Camera battleCam;
    public Text hpChangeText;
    public Text hpText;
    public GameObject butteredImage, bleedingImage;
    public Enemy thisEnemy;

    private BattleManager battleManager;
    private int health;
    private float currentHealth;
    private int strenght;
    private int initiative;
    private int type;
    private bool turnFinished = false;
    private int accuracy = 3;
    private int battleTurns = 0;
    private int accuracyChangedTurn = 0;
    private bool bleeding = false;
    private int bleedingStartedTurn = 0;
    private float position;
    private bool canHeal;

    // Start is called before the first frame update
    void Start()
    {
        battleManager = GameObject.FindGameObjectWithTag("BattleManager").GetComponent<BattleManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetUpBattleEnemy(Enemy enemy, float enemyPosition)
    {
        canHeal = enemy.canHeal;
        thisEnemy = enemy;
        
        Instantiate(enemy.image, this.gameObject.transform);
        if (enemy.rig != null)
        {
            Instantiate(enemy.rig, this.gameObject.transform);
        }

        health = enemy.health;
        currentHealth = health;
        strenght = enemy.strenght;
        initiative = enemy.initiative;
        type = enemy.type;
        float pos = enemyPosition == 0 ? 1 : 0.8f;
        pos = enemyPosition == 2 ? 0.9f : pos;
        position = pos;
        SetHealthBarPosition(position);
        hpText.text = currentHealth.ToString();
        this.gameObject.transform.localScale = new Vector3(0.1f, 0.2f, 0.1f);
        if (enemy.enemyName.Equals("Cobra Tape"))
        {
            Vector3 temp = this.gameObject.transform.position;
            temp.x = 3.2f;
            temp.y = -0.6f;
            this.gameObject.transform.position = temp;
        }
        else
        {
            Vector3 temp = this.gameObject.transform.position;
            temp.x -= 0.5f;
            temp.y = 0;
            this.gameObject.transform.position = temp;
        }
    }

    public Vector3 GetHealthBarPosition()
    {
        return healthBar.transform.position;
    }

    public int GetEnemyType()
    {
        return type;
    }

    public float GetEnemyPosition()
    {
        return position;
    }

    public void SetSelected(Color c)
    {
        border.color = c;
    }

    public void TakeDamage(float damage)
    {
        float x = metal.gameObject.GetComponent<RectTransform>().rect.width;
        float temp = damage / currentHealth * x;
        x -= (int)temp;
        currentHealth -= (int)damage;
        metal.gameObject.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, x);
        ChangeText(damage, true);

    }

    public void FinishedWaiting()
    {
        hpChangeText.text = "";
        if (currentHealth <= 0)
        {
            battleManager.DefeatedEnemy(position);
            Destroy(this.gameObject);
        }
    }

    private void ChangeText(float hpChange, bool isDamage)
    {
        string color = isDamage ? "red" : "green";
        string hp = "\n<color=" + color + ">" + (int)hpChange + "</color>";
        hpChangeText.text += hp;
        hpText.text = currentHealth.ToString();
    }

    private void SetHealthBarPosition(float position)
    {
        battleCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        Vector3 screenPos = battleCam.WorldToScreenPoint(this.gameObject.transform.position);
        screenPos.y -= (200 * position);
        screenPos.x -= 20;
        healthBar.transform.position = screenPos;
    }

    public void RaiseAccuracy(int i)
    {
        accuracy += i;
        accuracyChangedTurn = battleTurns;
        butteredImage.SetActive(true);
    }

    public void SetBleeding(bool i)
    {
        bleeding = i;
        if (i)
        {
            bleedingStartedTurn = battleTurns;
            bleedingImage.SetActive(true);
        }
    }

    public void DecideOnAction()
    {
        bool finished = false;
        int x = 3;
        if(thisEnemy.numberAttackStrengths.Length == 2)
        {
            x += 3;
        }
        accuracy += x;

        do
        {
            int i = Random.Range(0, accuracy);
            if (i > 0 && i < 4)
            {
                finished = AttackPlayer(0);
            }
            else if (i > 3 && i < 7)
            {
                finished = AttackPlayer(0);
            }
            else if (i < 1 && currentHealth < health && canHeal)
            {
                finished = Heal();
            }
            else if((i > 3 && thisEnemy.numberAttackStrengths.Length == 1) || (i > 6 && thisEnemy.numberAttackStrengths.Length == 2))
            {
                finished = true;
                hpChangeText.text = "Miss";
            }
        } while (!finished);
        if (bleeding)
        {
            int dam = currentHealth > 5 ? 5 : (1 - (int)currentHealth) * (-1);
            TakeDamage(dam);
        }
        if ((battleTurns - accuracyChangedTurn) > 2) { accuracy = 3; butteredImage.SetActive(false); }
        if ((battleTurns - bleedingStartedTurn) > 3) { bleeding = false; bleedingImage.SetActive(false); }
        battleTurns++;
        accuracy = 0;
    }

    private bool AttackPlayer(int attackNumber)
    {
        battleManager.AttackPlayer(thisEnemy.numberAttackStrengths[attackNumber], type);
        return true;
    }

    private bool Heal()
    {
        if(currentHealth == health)
        {
            return false;
        }
        else
        {
            float temp = 30.0f / (float)health * 200.0f;
            float x = metal.gameObject.GetComponent<RectTransform>().rect.width;
            float y = background.gameObject.GetComponent<RectTransform>().rect.width;
            x += (int)temp;
            currentHealth += 30;
            if(currentHealth > health)
            {
                currentHealth = health;
                x = y;
            }
            metal.gameObject.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, x);
            ChangeText(30, false);
            return true;
        }
        return false;
    }
}
