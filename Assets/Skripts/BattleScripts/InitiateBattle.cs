using UnityEngine;
using UnityEngine.SceneManagement;

public class InitiateBattle : MonoBehaviour
{
    public int index;
    public int numberEnemys;
    public int[] enemyIDs;
    public string battleSceneToLoad;
    public string originalScene;
    public EnemyList battleEnemyList;
    public EnemyList allEnemyList;
    public BattleScriptableObject data;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetIndex(int i)
    {
        index = i;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            battleEnemyList.ClearList();
            for (int i = 0; i < numberEnemys; i++)
            {
                battleEnemyList.AddItem(allEnemyList.GetItemByIndex(enemyIDs[i]));
            }
            data.gameManagerIndex = index;
            data.fight = true;
            data.originalScene = originalScene;
            data.originalPosition = this.gameObject.transform.localPosition;
            SceneManager.LoadScene(battleSceneToLoad, LoadSceneMode.Single);
        }
    }
}
