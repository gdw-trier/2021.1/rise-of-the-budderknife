using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class II_BattleManager : MonoBehaviour
{
    public GameObject playerPrefab;
    public GameObject indicator;
    public bool isBossBattle;
    public int numberEnemies;
    public Color selectColor;
    public GameObject canvas;
    public BattleScriptableObject battle;
    public GameObject level;
    public GameObject mainCamera;

    //0 = stumpf, 1 = buttrig, 2 = schneidend, 3 = durchdringend
    private float[,] typeTable = new float[4, 4];
    private List<II_BattleEnemy> battleEnemies;
    private List<GameObject> enemies;
    private Vector3[] enemyPos = new Vector3[3];
    private Vector3 playerPos = new Vector3(1043f, 359.7f, -533f);
    private int selectedEnemy = 0;
    private bool attackSelected;
    private int attackType = 0;
    private Vector3 restVector = new Vector3(0f, -700f, 0f);
    private BattlePlayerInput playerInput;
    private bool turnOver = false;
    private PlayerInput pInput;
    private bool finishedWaiting = false;
    private int currentEnemy = 0;
    private bool positioned = false;

    // Start is called before the first frame update
    void Start()
    {
        mainCamera.SetActive(false);
        Vector3 temp = new Vector3(4f, 0.6f, -5f);
        enemyPos[0] = temp;
        temp = new Vector3(3f, 0.6f, -4.4f);
        enemyPos[1] = temp;
        temp = new Vector3(2f, 0.6f, -3.4f);
        enemyPos[2] = temp;

        InitializeTypeTable();
        enemies = new List<GameObject>();
        DontDestroyOnLoad(mainCamera);
       // DontDestroyOnLoad(gameObject);
       // playerInput = this.gameObject.GetComponent<BattlePlayerInput>();
        pInput = this.gameObject.GetComponent<PlayerInput>();
    }

    // Update is called once per frame
    void Update()
    {
        if(battle.fight)
        {
            level.SetActive(false);
            canvas.SetActive(true);
            mainCamera.SetActive(true);
            if(!positioned)
            {
                PositionCharacters();
            }       
        }
        else
        {
            level.SetActive(true);
            canvas.SetActive(false);
            mainCamera.SetActive(false);
        }
        if (finishedWaiting)
        {
            finishedWaiting = false;
            WaitForSeconds();
        }
    }

    public void SetTurnOver(bool i)
    {
        DeselectEnemies();
        turnOver = i;
        StopPlayerInput(false);
        StartCoroutine(TurnWaitCoroutine());
    }

    public void WaitForSeconds()
    {
        Debug.Log("Waiting for Seconds " + numberEnemies + ", " + currentEnemy);
        finishedWaiting = false;
        playerInput.FinishedWaiting(currentEnemy);
        for (int i = 0; i < numberEnemies; i++)
        {
            battleEnemies[i].FinishedWaiting();
        }

        DeselectEnemies();
        if (currentEnemy == numberEnemies)
        {
            turnOver = false;
        }
        if (currentEnemy == 0)
        {
            Debug.Log("Selected first enemy");
            SelectEnemy(false);
        }
        if (currentEnemy > 0)
        {
            SelectEnemy(true);
        }

        if (turnOver)
        {
            EnemyTurn(currentEnemy);
        }
        else if (!turnOver)
        {
            pInput.enabled = true;
            playerInput.DeselectOptions();
            DeselectEnemies();
            currentEnemy = 0;
        }
    }

    //Die Coroutine, die kurz wartet und dann den Turn an den n�chsten Gegner �bergibt
    IEnumerator TurnWaitCoroutine()
    {
        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(2);
        finishedWaiting = true;
    }

    public void StopPlayerInput(bool i)
    {
        pInput.enabled = i;
    }

    public void EnemyTurn(int index)
    {
        battleEnemies[index].DecideOnAction();
        currentEnemy++;
        StartCoroutine(TurnWaitCoroutine());
    }

    public void DefeatedEnemy(float pos)
    {
        int index = 0;
        for (int i = 0; i < numberEnemies; i++)
        {
            if (battleEnemies[i].GetEnemyPosition() == pos)
            {
                index = i;
            }
        }
        enemies.RemoveAt(index);
        battleEnemies.RemoveAt(index);
        numberEnemies--;
        if(numberEnemies <= 0)
        {
            battle.fight = false;
            string s = SceneManager.GetSceneByName("2._Test_Battle").name;
            SceneManager.UnloadSceneAsync(s);
        }
    }

    public void PositionCharacters()
    {
        positioned = true;
        battleEnemies = new List<II_BattleEnemy>();
        GameObject.Instantiate(playerPrefab, playerPos, Quaternion.identity);
        for (int i = 0; i < numberEnemies; i++)
        {
            Instantiate(enemies[i], enemyPos[i], Quaternion.identity);
            battleEnemies.Add(enemies[i].GetComponent<II_BattleEnemy>());
        }
        //battleEnemies[0].SetPosition(1);
        //battleEnemies[1].SetPosition(0.9f);
        //battleEnemies[2].SetPosition(0.8f);
    }

    public void AttackSelectedEnemy(int strength)
    {
        int enemyType = battleEnemies[selectedEnemy].GetEnemyType();
        Debug.Log(attackType);
        Debug.Log(enemyType);
        float damageModifier = typeTable[attackType, enemyType];
        float rand = Random.Range(0.8f, 1.2f);
        float d = strength * damageModifier * rand;
        battleEnemies[selectedEnemy].TakeDamage(d);

        int upperLimit = isBossBattle ? 5 : 2;
        if (attackType == 1)
        {
            int i = Random.Range(0, upperLimit);
            int acc = i == 0 ? 8 : 3;
            acc = i == 1 ? 5 : acc;
            battleEnemies[selectedEnemy].SetAccuracy(acc);
        }

        if (attackType == 3)
        {
            int i = Random.Range(0, 2);
            bool acc = i == 0 ? true : false;
            battleEnemies[selectedEnemy].SetBleeding(acc);
        }
        DeselectEnemies();
    }

    public void AttackPlayer(int damage, int enemyType)
    {
        float damageModifier = typeTable[enemyType, 0];
        float rand = Random.Range(0.8f, 1.2f);
        float d = damage * damageModifier * rand;
        playerInput.TakeDamage((int)d);
    }

    public void DeselectAction()
    {
        attackType = -1;
        attackSelected = false;
        selectedEnemy = 0;
    }

    public void DeselectEnemies()
    {
        for (int i = 0; i < numberEnemies; i++)
        {
            battleEnemies[i].SetSelected(Color.black);
        }
        indicator.transform.position = restVector;
    }

    public void SelectEnemy(bool direction)
    {
        Debug.Log("SelectedEnemy1: " + selectedEnemy);
        battleEnemies[selectedEnemy].SetSelected(Color.black);
        if (direction && selectedEnemy < numberEnemies - 1)
        {
            selectedEnemy++;
        }
        else if (!direction && selectedEnemy > 0)
        {
            selectedEnemy--;
        }
        battleEnemies[selectedEnemy].SetSelected(selectColor);
        Vector3 screenPos = battleEnemies[selectedEnemy].GetHealthBarPosition();
        screenPos.y -= 55;
        indicator.transform.position = screenPos;
        Debug.Log("SelectedEnemy2: " + selectedEnemy + ", pos: " + indicator.GetComponent<RectTransform>().transform.position);
    }

    public void SetAttackType(int i)
    {
        attackType = i;
        Debug.Log("SetAttackType");
    }

    public void InitializeTypeTable()
    {
        for (int i = 0; i < 4; i++)
        {
            for (int l = 0; l < 4; l++)
            {
                if (i < 2 && l < 2)
                {
                    typeTable[i, l] = 1f;
                }
                else if (i > 1 && l > 1)
                {
                    typeTable[i, l] = 1f;
                }
            }
        }
        typeTable[0, 2] = 1.5f;
        typeTable[0, 3] = 0.5f;
        typeTable[1, 2] = 0.5f;
        typeTable[1, 3] = 1.5f;
        typeTable[2, 0] = 0.5f;
        typeTable[2, 1] = 1.5f;
        typeTable[3, 0] = 1.5f;
        typeTable[3, 1] = 0.5f;
    }

    public void addEnemies(GameObject enemy)
    {
        enemies.Add(enemy);
    }
}
