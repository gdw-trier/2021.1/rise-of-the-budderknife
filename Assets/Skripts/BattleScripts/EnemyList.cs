using UnityEngine;
using System.Collections.Generic;

public class EnemyList : ScriptableObject
{
    public List<Enemy> itemList;

    public void SetList(List<Enemy> x)
    {
        itemList = x;
    }

    public void ClearList()
    {
        itemList = new List<Enemy>();
    }

    public List<Enemy> GetList()
    {
        return itemList;
    }

    public void AddItem(Enemy item)
    {
        itemList.Add(item);
    }

    public void DeleteItem(Enemy item)
    {
        itemList.Remove(item);
    }

    public Enemy GetItemByIndex(int index)
    {
        return itemList[index];
    }

    public Enemy GetItem(string name)
    {
        for (int i = 0; i < itemList.Count; i++)
        {
            if (name.Equals(itemList[i].enemyName))
            {
                return itemList[i];
            }
        }
        return null;
    }

    public bool HasItem(string name)
    {
        for (int i = 0; i < itemList.Count; i++)
        {
            if (name.Equals(itemList[i].enemyName))
            {
                return true;
            }
        }
        return false;
    }
}
