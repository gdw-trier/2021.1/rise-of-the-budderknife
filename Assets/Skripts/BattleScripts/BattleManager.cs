using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class BattleManager : MonoBehaviour
{
    public GameObject playerPrefab;
    public GameObject enemyPrefab;
    public GameObject indicator;
    public GameObject playerIndicator;
    public bool isBossBattle;
    public int numberEnemies;
    public Color selectColor;
    public EnemyList currentEnemyList;
    public BattleScriptableObject data;
    public GameObject victoryScreen;
    public GameObject defeatScreen;
    public GameState gameState;

    //0 = stumpf, 1 = buttrig, 2 = schneidend, 3 = durchdringend
    private float[,] typeTable = new float[4, 4];
    private List<BattleEnemy> battleEnemies;
    private List<GameObject> enemies;
    private GameObject player;
    private Vector3[] enemyPos = new Vector3[3];
    private Vector3 playerPos = new Vector3(1043f, 359.7f, -533f);
    private int selectedEnemy = 0;
    private bool attackSelected;
    private int attackType = 0;
    private Vector3 restVector = new Vector3(0f, -700f, 0f);
    private BattlePlayerInput playerInput;
    private bool turnOver = false;
    private PlayerInput pInput;
    private bool finishedWaiting = false;
    private int currentEnemy = 0;

    // Start is called before the first frame update
    void Start()
    {
        Vector3 temp = new Vector3(4f, 0.6f, -5f);
        enemyPos[0] = temp;
        temp = new Vector3(3f, 0.6f, -4.4f);
        enemyPos[1] = temp;
        temp = new Vector3(2f, 0.6f, -3.4f);
        enemyPos[2] = temp;

        numberEnemies = currentEnemyList.itemList.Count;
        InitializeTypeTable();
        PositionCharacters();
        playerInput = this.gameObject.GetComponent<BattlePlayerInput>();
        pInput = this.gameObject.GetComponent<PlayerInput>();
        playerInput.SetHealthAndButter(gameState.health, gameState.butter);
        playerInput.SetButterUse(gameState.canUseButter);
        playerInput.SetSawUse(gameState.canUseSaw);
        playerInput.SetJumpUse(gameState.canUseJump);
    }

    // Update is called once per frame
    void Update()
    {
        if (finishedWaiting)
        {
            finishedWaiting = false;
            if(numberEnemies == 0)
            {
                ExitBattle();
            }
            else
            {
                WaitForSeconds();
            }
        }
    }

    //Wird vom Spieler aufgerufen, wenn dieser eine Aktion ausgeführt hat
    public void SetTurnOver(bool i)
    {
        playerIndicator.SetActive(false);
        DeselectEnemies();
        turnOver = i;
        StopPlayerInput(false);
        StartCoroutine(TurnWaitCoroutine());
    }

    private void WonBattle()
    {
        victoryScreen.SetActive(true);
        float health = playerInput.GetCurrentHealth();
        float butter = playerInput.GetCurrentButter();
        gameState.health = (int)health;
        gameState.butter = (int)butter;
        player.SetActive(false);
        data.won = true;
        data.enemyAlive[data.gameManagerIndex] = true;
        StopPlayerInput(false);
        StartCoroutine(TurnWaitCoroutine());
    }

    public void LostBattle()
    {
        battleEnemies = new List<BattleEnemy>();
        numberEnemies = 0;
        defeatScreen.SetActive(true);
        float health = playerInput.GetCurrentHealth();
        float butter = playerInput.GetCurrentButter();
        gameState.health = (int)health;
        gameState.butter = (int)butter;
        player.SetActive(false);
        data.enemyAlive[data.gameManagerIndex] = false;
        StopPlayerInput(false);
        StartCoroutine(TurnWaitCoroutine());
    }

    private void ExitBattle()
    {
        SceneManager.LoadScene(data.originalScene, LoadSceneMode.Single);
    }

    //Nach Ablauf der Coroutine setzt diese Methode allen Schaden 
    //zurück und entscheidet, wer als nächstes dran ist
    private void WaitForSeconds()
    {
        Debug.Log("Waiting for Seconds " + numberEnemies + ", " + currentEnemy);
        finishedWaiting = false;
        playerInput.FinishedWaiting(currentEnemy);
        for(int i = 0; i < numberEnemies; i++)
        {
            battleEnemies[i].FinishedWaiting();
        }

        DeselectEnemies();
        if (currentEnemy == numberEnemies)
        {
            turnOver = false;
        }
        if(currentEnemy == 0)
        {
            Debug.Log("Selected first enemy");
            SelectEnemy(false);
        }
        if(currentEnemy > 0)
        {
            SelectEnemy(true);
        }

        if (turnOver)
        {
            EnemyTurn(currentEnemy);
        }
        else if (!turnOver)
        {
            pInput.enabled = true;
            playerInput.DeselectOptions();
            DeselectEnemies();
            currentEnemy = 0;
            playerIndicator.SetActive(true);
        }
    }

    //Die Coroutine, die kurz wartet und dann den Turn an den nächsten Gegner übergibt
    IEnumerator TurnWaitCoroutine()
    {
        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(2);
        finishedWaiting = true;
    }

    //Stoppt den Input vom SPieler
    public void StopPlayerInput(bool i)
    {
        pInput.enabled = i;
    }

    //Ruft den entsprechenden Gegner auf und wartet anschließend kurze Zeit
    public void EnemyTurn(int index)
    {
        battleEnemies[index].DecideOnAction();
        currentEnemy++;
        StartCoroutine(TurnWaitCoroutine());
    }

    //Nehmen einen besiegten Gegner aus den Arrays raus
    public void DefeatedEnemy(float pos)
    {
        int index = 0;
        for(int i = 0; i < numberEnemies; i++)
        {
            if(battleEnemies[i].GetEnemyPosition()  == pos)
            {
                index = i;
            }
        }
        enemies.RemoveAt(index);
        battleEnemies.RemoveAt(index);
        numberEnemies--;
        if (numberEnemies == 0)
        {
            WonBattle();
        }
    }

    //Positioniert zu Beginn des Spiels die Gegner und liest sie ein
    private void PositionCharacters()
    {
        enemies = new List<GameObject>();
        battleEnemies = new List<BattleEnemy>();
        player = GameObject.Instantiate(playerPrefab, playerPos, Quaternion.identity);
        for(int i = 0; i < numberEnemies; i++)
        {
            GameObject temp = enemyPrefab;
            enemies.Add(GameObject.Instantiate(enemyPrefab, enemyPos[i], Quaternion.identity));
            battleEnemies.Add(enemies[i].GetComponent<BattleEnemy>());
            battleEnemies[i].SetUpBattleEnemy(currentEnemyList.GetItemByIndex(0), i);
        }
    }

    //Greift den zurzeit angewählten Gegner mit einer übergebenen Stärke an
    //und gibt bei Zusatzeffekten an, ob sie gewirkt haben und wenn, wie stark
    public void AttackSelectedEnemy(int strength)
    {
        int enemyType = battleEnemies[selectedEnemy].GetEnemyType();
        float damageModifier = typeTable[attackType, enemyType];
        float rand = Random.Range(0.8f, 1.2f);
        int d = (int)(strength * damageModifier * rand);
        battleEnemies[selectedEnemy].TakeDamage(d);

        int upperLimit = isBossBattle ? 5 : 2;
        if(attackType == 1)
        {
            int i = Random.Range(0, upperLimit);
            int acc = i == 0 ? 5 : 0;
            acc = i == 1 ? 2 : acc;
            battleEnemies[selectedEnemy].RaiseAccuracy(acc);
        }

        if (attackType == 3)
        {
            int i = Random.Range(0, 2);
            bool acc = i == 0 ? true : false;
            battleEnemies[selectedEnemy].SetBleeding(acc);
        }
        DeselectEnemies();
    }

    //Angegebener Enemy Typ greift den Spieler mit einer bestimmten Stärke an
    public void AttackPlayer(int damage, int enemyType)
    {
        float damageModifier = typeTable[enemyType, 0];
        float rand = Random.Range(0.8f, 1.2f);
        int d = (int)(damage * damageModifier * rand);
        playerInput.TakeDamage(d);
    }

    //Deselektiert eine angewählte Aktion w
    public void DeselectAction()
    {
        attackType = -1;
        attackSelected = false;
        selectedEnemy = 0;
    }

    //Deselektiert alle Gegner
    public void DeselectEnemies()
    {
        for(int i = 0; i < numberEnemies; i++)
        {
            battleEnemies[i].SetSelected(Color.white);
        }
        indicator.transform.position = restVector;
    }

    //Wählt den nächsten Gegner in einer bestimmten Richtung aus
    public void SelectEnemy(bool direction)
    {
        if (battleEnemies.Count == 0)
        {
            return;
        }
        battleEnemies[selectedEnemy].SetSelected(Color.white);
        if(direction && selectedEnemy < numberEnemies-1)
        {
            selectedEnemy++;
        }
        else if(!direction && selectedEnemy > 0)
        {
            selectedEnemy--;
        }
        battleEnemies[selectedEnemy].SetSelected(selectColor);
        Vector3 screenPos = battleEnemies[selectedEnemy].GetHealthBarPosition();
        screenPos.y -= 75;
        indicator.transform.position = screenPos;
    }

    //Der Spieler setzt den Angriffstyp seiner ausgewählten Attacke
    public void SetAttackType(int i)
    {
        attackType = i;
    }

    //Der Spieler bekommt den Angriffstyp seiner ausgewählten Attacke
    public int GetAttackType()
    {
        return attackType;
    }

    //Initialisiert alle Typenabhängigkeiten
    private void InitializeTypeTable()
    {
        for(int i = 0; i < 4; i++)
        {
            for(int l = 0; l < 4; l++)
            {
                if(i < 2 && l < 2)
                {
                    typeTable[i, l] = 1f;
                }
                else if(i > 1 && l > 1)
                {
                    typeTable[i, l] = 1f;
                }
            }
        }
        typeTable[0, 2] = 1.5f;
        typeTable[0, 3] = 0.5f;
        typeTable[1, 2] = 0.5f;
        typeTable[1, 3] = 1.5f;
        typeTable[2, 0] = 0.5f;
        typeTable[2, 1] = 1.5f;
        typeTable[3, 0] = 1.5f;
        typeTable[3, 1] = 0.5f;
    }

    //Fügt Gegner zum BattleManager hinzu
    public void addEnemies(GameObject enemy)
    {
        enemies.Add(enemy);
    }
}
