using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]                         
public class Enemy
{
    public string enemyName = "New Enemy";     
    public GameObject image;
    public GameObject rig;
    public int health;
    public int strenght;
    public int initiative;
    public int type;
    public int[] numberAttackStrengths;
    public bool canHeal;
    public float xPosition;
    public float yPosition;
}
