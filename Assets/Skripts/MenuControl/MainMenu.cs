using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject pauseMenu, optionsMenu, creditsMenu;

    public GameObject pauseFirstButton,
        optionsFirstButton,
        optionsClosedButton,
        creditsFirstButton,
        creditsClosedButton;
    public BattleScriptableObject data;
    public GameState state;

    public void Play()
    {
        for(int i = 0; i < data.enemyAlive.Length; i++)
        {
            data.enemyAlive[i] = false;
        }
        data.finishedStartCutscene = false;
        state.health = 100;
        state.butter = 60;
        SceneManager.LoadScene("FirstLevel");
        StartCoroutine(WaitTimeScale());
    }

    //In MainMenu to quit Game
    public void QuitGame()
    {
        //Debug.Log("Quit!");
        Application.Quit();
    }

    //From PauseMenu to MainMenu
    public void BackToMain()
    {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(pauseFirstButton);
        SceneManager.LoadScene("MainMenu");
    }

    public void PauseUnpause()
    {
        if (!pauseMenu.activeInHierarchy)
        {
            pauseMenu.SetActive(true);
            Time.timeScale = 0f;
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(pauseFirstButton);
        }
        else
        {
            pauseMenu.SetActive(false);
            optionsMenu.SetActive(false);
            creditsMenu.SetActive(false);
            StartCoroutine(WaitTimeScale());
        }
    }

    public void OpenOptions()
    {
        optionsMenu.SetActive(true);
        pauseMenu.SetActive(false);

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(optionsFirstButton);
    }

    public void CloseOptions()
    {
        optionsMenu.SetActive(false);
        pauseMenu.SetActive(true);

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(optionsClosedButton);
    }

    public void OpenCredits()
    {
        creditsMenu.SetActive(true);
        pauseMenu.SetActive(false);

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(creditsFirstButton);
    }

    public void CloseCredits()
    {
        creditsMenu.SetActive(false);
        pauseMenu.SetActive(true);

        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(creditsClosedButton);
    }

    private IEnumerator WaitTimeScale()
    {
        yield return new WaitForSecondsRealtime(0.2f);
        Time.timeScale = 1f;
        GameObject.FindWithTag("Player").GetComponent<PlayerMovement>().paused = false;
    }
}