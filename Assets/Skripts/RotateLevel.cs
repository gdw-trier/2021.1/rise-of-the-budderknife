using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateLevel : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        transform.rotation = Quaternion.Euler(0, -90, 0);
        GetComponent<Collider>().enabled = false;
    }
}
