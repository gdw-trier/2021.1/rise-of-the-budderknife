using System.Collections;
using UnityEngine;

public class ButterSlide : MonoBehaviour
{
    public Animator animator;
    public GameManager manager;
    
    private Rigidbody body;
    private PlayerMovement PlayerMove;
    [SerializeField] private Transform Spawnpos;
    [SerializeField] private float SlidingSpeed = 2;
    [SerializeField] private float Duration = 0.5f;
    [SerializeField] private GameObject Butter;
    [SerializeField] private float ButterCapacity;
    private float ButterAmount;
    [SerializeField] private AudioSource dropButter;

    private void Awake()
    {
        ButterAmount = manager.GetButter();
        body = gameObject.GetComponent<Rigidbody>();
        PlayerMove = gameObject.GetComponent<PlayerMovement>();
    }

    private void OnButterSlide()
    {
        if (ButterAmount > 0)
        {
            dropButter.Play();
            Instantiate(Butter, Spawnpos.position, Quaternion.identity);
            Debug.Log("Butter");
            ButterAmount--;
            manager.ChangeButterBar(1, false);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Butterstain"))
        {
            animator.SetBool("sliding", true);
            Debug.Log("Slide");
            PlayerMove.speed *= SlidingSpeed;
            body.useGravity = false;
            StartCoroutine(Wait(Duration));
        }
    }

    private IEnumerator Wait(float time)
    {
        yield return new WaitForSeconds(time);
        PlayerMove.speed /= SlidingSpeed;
        body.useGravity = true;
        animator.SetBool("sliding", false);
        //Debug.Log("WAITIINNGG");
    }
}