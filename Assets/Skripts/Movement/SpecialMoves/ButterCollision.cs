using UnityEngine;

public class ButterCollision : MonoBehaviour
{
    private Rigidbody butterBody;
    private void Awake()
    {
        butterBody = gameObject.GetComponent<Rigidbody>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            Debug.Log("Trigger");
            Debug.Log(butterBody.velocity.magnitude);
            butterBody.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY |
                                      RigidbodyConstraints.FreezePositionZ;
            butterBody.constraints = RigidbodyConstraints.FreezeAll | RigidbodyConstraints.FreezeAll;
        }
    }
}
