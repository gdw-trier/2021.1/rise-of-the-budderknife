using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class Saw : MonoBehaviour
{
    public bool CanSaw = false;
    private bool NearToSawable = false;
    [SerializeField] private GameObject hitbox;
    private GameObject Sawable;
    
    public void OnSaw()
    {
        if (CanSaw && NearToSawable)
        {
            //TODO: Sawing Animation & Delay 
            //Debug.Log("Sawing");
            
            Destroy(Sawable);
            NearToSawable = false;
            //Debug.Log("Destroyed: Sawable = false");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Sawable"))
        {
            Sawable = other.gameObject;
            NearToSawable = true;
            //Debug.Log("TriggerEnter: Sawable = true");
        }
        else
        {
            NearToSawable = false;
            //Debug.Log("TriggerEnter(else): Sawable = false");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        NearToSawable = false;
        //Debug.Log("TriggerExit: Sawable = false");
    }
}
