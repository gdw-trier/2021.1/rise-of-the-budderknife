using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    public Transform Pivot;
    public Animator animator;
    
    //general Movement
    public float speed;
    private Vector2 dir;
    private Rigidbody body;
    public bool LookRight = true;

    //Jump
    private bool grounded = true;
    public bool CanJump = false;
    [SerializeField] private float jumpforce;
    [SerializeField] private float fallforce;

    public bool paused = false;
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private GameObject pauseFirstButton;


    void Start()
    {
        body = GetComponent<Rigidbody>();
    }

    private void OnPauseUnpause()
    { //if Game = active load PauseMenu, else load Game
        if (!pauseMenu.activeInHierarchy)
        {
            pauseMenu.SetActive(true);
            Time.timeScale = 0f;
            EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(pauseFirstButton);
            paused = true;
        }
        else
        {
            pauseMenu.SetActive(false);
            Time.timeScale = 1f;
        }
        
    }

    private void OnMove(InputValue action)
    {
        dir = action.Get<Vector2>();
        animator.SetFloat("velocity", Mathf.Abs(dir.x));
        if (dir.x > 0)
        {
            if (!LookRight)
            {
                LookRight = true;
                Vector3 theScale = Pivot.localScale;
                theScale.x *= -1;
                Pivot.localScale = theScale;
            }
        }
        else if (dir.x < 0)
        {
            if (LookRight)
            {
                LookRight = false;
                Vector3 theScale = Pivot.localScale;
                theScale.x *= -1;
                Pivot.localScale = theScale;
            }
        }
    }

    //Player can jump after he gets the special ability and e.g. touches the ground (stops flying)
    public void OnJump()
    {
        if (CanJump && grounded && !paused)
        {
            body.AddForce(Vector3.up * jumpforce, ForceMode.Impulse);
            grounded = false;
            animator.SetBool("grounded", false);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            grounded = true;
            animator.SetBool("grounded", true);
        }
    }

    void FixedUpdate()
    {
        Vector3 relativeDir = (transform.forward * dir.y + transform.right * dir.x).normalized * speed;
        Vector3 velo = new Vector3(relativeDir.x, body.velocity.y, relativeDir.z);

        if (velo.y < 0)
        {
            velo.y += (fallforce - 1) * Physics.gravity.y * Time.fixedDeltaTime;
        }

        body.velocity = velo;
    }
}