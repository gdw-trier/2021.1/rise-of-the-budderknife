using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Cutscene : MonoBehaviour
{
    public Sprite[] scenes;
    public Image basic;
    public int timeToWait;
    public GameObject manager;
    public PlayerMovement player;
    public bool isStartingCutscene;
    public BattleScriptableObject data;

    private int currentPicture = 0;
    private bool finishedShowing;

    private void Start()
    {
        Time.timeScale = 1f;
        if (isStartingCutscene && !data.finishedStartCutscene)
        {
            manager.SetActive(false);
            player.enabled = false;
            basic.color = new Color(255, 255, 255, 255);
            data.finishedStartCutscene = true;
            ShowPicture();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (finishedShowing)
        {
            finishedShowing = false;
            ShowPicture();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            manager.SetActive(false);
            player.enabled = false;
            basic.color = new Color(255, 255, 255, 255); ;
            ShowPicture();
        }
    }

    private void ShowPicture()
    {
        Debug.Log("Show next picture");
        if(currentPicture == scenes.Length)
        {
            basic.sprite = null;
            manager.SetActive(true);
            player.enabled = true;
            Destroy(this.gameObject);
        }
        else
        {
            basic.sprite = scenes[currentPicture];
        }

        currentPicture++;
        StartCoroutine(TurnWaitCoroutine());
    }

    //Die Coroutine, die kurz wartet und dann den Turn an den n�chsten Gegner �bergibt
    IEnumerator TurnWaitCoroutine()
    {
        Debug.Log("Waiting " + timeToWait);
        //yield on a new YieldInstruction that waits for 5 seconds.
        yield return new WaitForSeconds(timeToWait);
        finishedShowing = true;
        Debug.Log("Finished");
    }
}
