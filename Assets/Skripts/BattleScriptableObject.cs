using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Fightactive", order = 1)]
public class BattleScriptableObject : ScriptableObject
{
    public bool fight;
    public bool won;
    public string originalScene;
    public Vector3 originalPosition;
    public int gameManagerIndex;
    public bool[] enemyAlive;
    public bool finishedStartCutscene;
}
