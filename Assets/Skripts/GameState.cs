using UnityEngine;

[CreateAssetMenu(fileName = "GameState", menuName = "ScriptableObjects/GameState", order = 1)]
public class GameState : ScriptableObject
{
    public int health;
    public int butter;
    public bool canUseButter;
    public bool canUseSaw;
    public bool canUseJump;
}
