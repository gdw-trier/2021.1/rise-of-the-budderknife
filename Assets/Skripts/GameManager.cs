using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject player;
    public GameObject[] enemies;
    public Vector3[] checkPoints;
    public BattleScriptableObject data;
    public Vector3 deadPosition = new Vector3(-1000, -1000, -1000);
    public GameObject healthBar, butterBar;
    public GameState gameState;

    private float healthBarWidth;
    private float butterBarWidth;

    // Start is called before the first frame update
    void Start()
    {
        healthBarWidth = healthBar.gameObject.GetComponent<RectTransform>().rect.width;
        butterBarWidth = butterBar.gameObject.GetComponent<RectTransform>().rect.width;
        PositionPlayer();
        SetUpEnemys();
        SetUpBars();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public int GetButter()
    {
        return gameState.butter;
    }

    public void ChangeButterBar(int change, bool raise)
    {
        if(raise)
        {
            gameState.butter += change;
            if (gameState.butter > 60) { gameState.butter = 60; }
        }
        else
        {
            gameState.butter -= change;
            if (gameState.butter < 0) { gameState.butter = 0; }
        }
        SetUpBars();
    }

    public void ChangeHealthBAr(int change, bool raise)
    {
        if (raise)
        {
            gameState.health += change;
            if(gameState.health > 100) { gameState.health = 100; }
        }
        else
        {
            gameState.health -= change;
        }
        SetUpBars();
    }

    public void HealUp()
    {
        gameState.health = 100;
        gameState.butter = 60;
        SetUpBars();
    }

    private void SetUpBars()
    {
        float x = healthBar.gameObject.GetComponent<RectTransform>().rect.width;
        float temp = (float)gameState.health / 100.0f * healthBarWidth;
        x = temp;
        if(x > healthBarWidth) { x = healthBarWidth; }
        healthBar.gameObject.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, x);

        float y = healthBar.gameObject.GetComponent<RectTransform>().rect.width;
        float tempe = (float)gameState.butter / 60.0f * butterBarWidth;
        y = tempe;
        if (y > butterBarWidth) { y = butterBarWidth; }
        butterBar.gameObject.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, y);
    }

    private void SetUpEnemys()
    {
        Debug.Log("Setting up Enemys");
        for(int i = 0; i < enemies.Length; i++)
        {
            if (data.enemyAlive[i])
            {
                enemies[i].transform.position = deadPosition;
                enemies[i].GetComponent<InitiateBattle>().SetIndex(i);
            }
        }
    }

    private void PositionPlayer()
    {
        if (data.fight && data.won)
        {
            player.transform.position = data.originalPosition;
        }
        else if (data.fight)
        {
            player.transform.position = checkPoints[GetNearestCheckpoint()];
        }
        else
        {
            Debug.Log("Position Player at STartPoint");
            player.transform.position = checkPoints[0];
        }
        data.won = false;
        data.fight = false;
    }

    private int GetNearestCheckpoint()
    {
        float xDiff = 1;
        int l = -2;
        for(int i = 0; xDiff >= 0; i++)
        {
            l++;
            xDiff = data.originalPosition.x - checkPoints[i].x;
        }
        Debug.Log(l);
        return l;
    }
}
