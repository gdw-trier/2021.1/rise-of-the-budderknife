using UnityEngine;
using UnityEngine.SceneManagement;

public class BetrunkenerKorken : MonoBehaviour
{
    bool torkeln = false;
    int steps = 0;
    public int speed = 1;
    public int strenght = 15;
    public int health = 70;
    public int typ = 0;
    public int kind = 1;
    public int walkdistance = 3;
    bool seeKnife = false;
    Vector3 startPosition;
    Vector3 goal;
    float pointA;
    float pointB;
    GameObject player;
    GameObject battleManager;
    GameObject itselfe;
    public GameObject canvas;
    public GameObject[] entcounter;
    public BattleScriptableObject battle;

    // Start is called before the first frame update
    void Start()
    {
        itselfe = gameObject;
        startPosition = itselfe.transform.position;
        pointA = startPosition.x + walkdistance;
        pointB = startPosition.x - walkdistance;
        goal = new Vector3(pointA, gameObject.transform.position.y, gameObject.transform.position.z);
        player = GameObject.FindGameObjectWithTag("Player");
        battleManager = GameObject.FindGameObjectWithTag("BattleManager");
       // itselfe.GetComponent<II_BattleEnemy>().SetUpBattleEnemy(null, health, strenght, speed, typ, kind);
    }

    private void FixedUpdate()
    {
            float step = speed * Time.fixedDeltaTime;
            if (player != null)
            {
                Vector3 dis;
                dis = itselfe.transform.position - player.transform.position;
                if (dis.magnitude <= 3.5)
                {
                    seeKnife = true;
                }
                else
                {
                    seeKnife = false;
                }
            }
            if (seeKnife)
            {
                goal = new Vector3(player.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
                Walk(goal, step);
            }
            else
            {
                float x = itselfe.transform.position.x;
                if (x >= pointA)
                {
                    goal = new Vector3(pointB, gameObject.transform.position.y, gameObject.transform.position.z);
                    Vector3 theScale = itselfe.transform.localScale;
                    theScale.x *= -1;
                    itselfe.transform.localScale = theScale;
                }
                else if (x <= pointB)
                {
                    goal = new Vector3(pointA, gameObject.transform.position.y, gameObject.transform.position.z);
                    Vector3 theScale = itselfe.transform.localScale;
                    theScale.x *= -1;
                    itselfe.transform.localScale = theScale;
                }
                Walk(goal, step);
            }
    }

    void Walk(Vector3 goal, float step)
    {
        if(torkeln)
        {
            itselfe.transform.position = Vector3.MoveTowards(gameObject.transform.position, goal,(-0.75f * step));
            steps++;
            if (steps == 10)
            {
                torkeln = !torkeln;
                steps = 0;
            }
        }
        else
        {
            itselfe.transform.position = Vector3.MoveTowards(gameObject.transform.position, goal, step);
            steps++;
            if (steps == 25)
            {
                torkeln = !torkeln;
                steps = 0;
            }
        }
    }

    public void AttackOne()
    {
        battle.fight = true;
        Vector3 attackPosition = new Vector3(player.transform.position.x, itselfe.transform.position.y, player.transform.position.z);
        while (Mathf.Abs(itselfe.transform.position.x - player.transform.position.x) > 0.5f)
        {
            Walk(attackPosition, (speed * Time.fixedDeltaTime));
        }
    }

    // private void OnCollisionEnter(Collision collision)
    // {
    //     if (collision.gameObject.CompareTag("Player"))
    //     {
    //         if (battle.fight)
    //         {
    //             Vector3 dis;
    //             dis = itselfe.transform.position - startPosition;
    //             while (itselfe.transform.position != startPosition)
    //             {
    //                 Walk(startPosition, (speed * Time.fixedDeltaTime));
    //             }
    //         }
    //         else
    //         {
    //             int j = entcounter.Length;
    //             battleManager.GetComponent<II_BattleManager>().numberEnemies = j;
    //             Time.timeScale = 0;
    //             foreach (GameObject i in entcounter)
    //             {
    //                 battleManager.GetComponent<II_BattleManager>().addEnemies(i);
    //             }
    //             battle.fight = true;
    //             SceneManager.LoadScene("2._Test_Battle", LoadSceneMode.Additive);
    //             SceneManager.SetActiveScene(SceneManager.GetSceneByName("2._Test_Battle"));
    //         }
    //     }
    // }
    //
    // public void OnCollisionExit(Collision collision)
    // {
    //     if (collision.gameObject.CompareTag("Player"))
    //     {
    //         Debug.Log("Destroy");
    //         //Destroy(itselfe);
    //     }
    // }
}
