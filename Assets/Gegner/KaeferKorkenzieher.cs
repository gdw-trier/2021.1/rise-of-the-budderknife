using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KaeferKorkenzieher : MonoBehaviour
{
    public int speed = 3;
    public int strenght = 23;
    public int health = 75;
    public int typ = 0;
    public int kind = 3;
    Vector3 startPosition;
    GameObject player;
    GameObject battleManager;
    GameObject itselfe;
    public GameObject fallingObject;
    public GameObject canvas;
    public GameObject[] entcounter;
    public BattleScriptableObject battle;

    // Start is called before the first frame update
    void Start()
    {
        itselfe = gameObject;
        startPosition = itselfe.transform.position;
        player = GameObject.FindGameObjectWithTag("Player");
        battleManager = GameObject.FindGameObjectWithTag("BattleManager");
        itselfe.GetComponent<II_BattleEnemy>().SetUpBattleEnemy(null, health, strenght, speed, typ, kind);
    }

    private void FixedUpdate()
    {
        if (!battle.fight)
        {
            canvas.SetActive(false);
        }
        else
        {
            canvas.SetActive(true);
            Time.timeScale = 1;
        }
    }

    void Walk(Vector3 goal, float step)
    {
        itselfe.transform.position = Vector3.MoveTowards(gameObject.transform.position, goal, step);
    }

    public void AttackOne()
    {
        Vector3 attackPosition = new Vector3(player.transform.position.x, itselfe.transform.position.y, player.transform.position.z);
        while (Mathf.Abs(itselfe.transform.position.x - player.transform.position.x) > 0.5f)
        {
            Walk(attackPosition, (speed * Time.fixedDeltaTime));
        }
    }

    public void AttackTwo()
    {
        Vector3 pos1 = new Vector3(player.transform.position.x, player.transform.position.y + 10, player.transform.position.z);
        Instantiate(fallingObject, pos1, Quaternion.identity);
        StartCoroutine("SpawnTwo");
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (battle.fight)
            {
                while (itselfe.transform.position != startPosition)
                {
                    Walk(startPosition, (speed * Time.fixedDeltaTime));
                }
            }
            else
            {
                int j = entcounter.Length;
                battleManager.GetComponent<II_BattleManager>().numberEnemies = j;
                Time.timeScale = 0;
                foreach (GameObject i in entcounter)
                {
                    battleManager.GetComponent<II_BattleManager>().addEnemies(i);
                }
                battle.fight = true;
                SceneManager.LoadScene("2._Test_Battle", LoadSceneMode.Additive);
            }
        }
    }

    public void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("Destroy");
            //Destroy(itselfe);
        }
    }

    IEnumerator SpawnTwo()
    {
        yield return new WaitForSeconds(0.5f);
        Vector3 pos2 = new Vector3(player.transform.position.x + 3, player.transform.position.y + 10, player.transform.position.z);
        Instantiate(fallingObject, pos2, Quaternion.identity);
        StartCoroutine("SpawnThree");
    }

    IEnumerator SpawnThree()
    {
        yield return new WaitForSeconds(0.5f);
        Vector3 pos3 = new Vector3(player.transform.position.x - 3, player.transform.position.y + 10, player.transform.position.z);
        Instantiate(fallingObject, pos3, Quaternion.identity);
    }
}
