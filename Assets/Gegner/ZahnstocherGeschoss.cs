using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZahnstocherGeschoss : MonoBehaviour
{
    float damage;
    GameObject player;
    Vector3 target;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        target = new Vector3(player.transform.position.x, gameObject.transform.position.y, player.transform.position.z);   
    }

    private void FixedUpdate()
    {
        float step = 4 * Time.fixedDeltaTime;
        gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, target, step);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
        }
    }
}
