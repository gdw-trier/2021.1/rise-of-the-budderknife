using UnityEngine;
using UnityEngine.SceneManagement;

public class MafiaZahnstocher : MonoBehaviour
{
    [SerializeField] private float detectionDistance;
    public int speed = 2;
    public int walkdistance = 2;
    bool seeKnife = false;
    Vector3 startPosition;
    Vector3 goal;
    float pointA;
    float pointB;
    GameObject player;
    GameObject itselfe;
    public GameObject projectile;
    public GameObject[] entcounter;
    public BattleScriptableObject battle;

    // Start is called before the first frame update
    void Start()
    {
        itselfe = gameObject;
        startPosition = itselfe.transform.position;
        pointA = startPosition.x + walkdistance;
        pointB = startPosition.x - walkdistance;
        goal = new Vector3(pointA, gameObject.transform.position.y, gameObject.transform.position.z);
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void FixedUpdate()
    {
        float step = speed * Time.fixedDeltaTime;
        if (player != null)
        {
            Vector3 dis;
            dis = itselfe.transform.position - player.transform.position;
            if (dis.magnitude <= detectionDistance)
            {
                seeKnife = true;
            }
            else
            {
                seeKnife = false;
            }
        }

        if (seeKnife)
        {
            goal = new Vector3(player.transform.position.x, gameObject.transform.position.y,
                gameObject.transform.position.z);
            Walk(goal, step);
        }
        else
        {
            float x = itselfe.transform.position.x;
            if (x >= pointA)
            {
                goal = new Vector3(pointB, gameObject.transform.position.y, gameObject.transform.position.z);
                Vector3 theScale = itselfe.transform.localScale;
                theScale.x *= -1;
                itselfe.transform.localScale = theScale;
            }
            else if (x <= pointB)
            {
                goal = new Vector3(pointA, gameObject.transform.position.y, gameObject.transform.position.z);
                Vector3 theScale = itselfe.transform.localScale;
                theScale.x *= -1;
                itselfe.transform.localScale = theScale;
            }

            Walk(goal, step);
        }
    }

    void Walk(Vector3 goal, float step)
    {
        itselfe.transform.position = Vector3.MoveTowards(gameObject.transform.position, goal, step);
    }

    public void AttackOne()
    {
        battle.fight = true;
        Vector3 attackPosition = new Vector3(player.transform.position.x, itselfe.transform.position.y,
            player.transform.position.z);
        while (Mathf.Abs(itselfe.transform.position.x - player.transform.position.x) > 0.5f)
        {
            Walk(attackPosition, (speed * Time.fixedDeltaTime));
        }
    }

    public void AttackTwo()
    {
        battle.fight = true;
        GameObject clone = Instantiate(projectile, itselfe.transform.position, Quaternion.identity);
    }
}